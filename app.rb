require 'rubygems'
require 'sinatra'
require 'sqlite3'

def init_db 
	@db =SQLite3::Database.new 'lepra.db'
	@db.results_as_hash = true
end

# before вызывается каждый раз при перезагрузке страницы
before do 
# инициализация БД
	init_db
end

# configure вызывается каждый раз при конфигурации приложения:
# когда изменился код и перезагрузилась таблица
configure do 
	# инициализация БД
	init_db
	# создает таблицу если таблица не существует
	@db.execute 'create table if not exists posts
	(
		id integer primary key autoincrement,
		created_date date, 
		content text
	)'

	@db.execute 'create table if not exists comments
	(
		id integer primary key autoincrement,
		created_date date, 
		content text,
		post_id integer
	)'
end

get '/' do

	@results = @db.execute 'select * from posts order by id desc'

	erb :index			
end

get '/new' do
  erb :new
end

post '/new' do
	# получаем переменную из post запроса
  content = params[:content]

	if content.size <= 0 
		@error = 'Введите текст'
		return erb :new
	end
	#сохранение данных в БД
	@db.execute 'insert into posts (content, created_date) values (?, datetime())', [content]

	redirect '/'
end

get '/details/:post_id' do 
	post_id = params[:post_id]

	results = @db.execute 'select * from posts where id = ?', [post_id]

	@row = results[0]

	@comments = @db.execute 'select * from comments where post_id = ? order by id', [post_id]

	erb :details
end

post '/details/:post_id' do 

	post_id = params[:post_id]

  content = params[:content]

	@db.execute 'insert into comments 
	(
		content, 
		created_date, 
		post_id
		) 
		values 
		(?, 
		datetime(),
		 ?
		 )', [content, post_id]

	redirect to'/details/' + post_id

end